package com.selenium.test;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DrupalLoginPage {

    WebDriver driver;
    By userName = By.id ("edit-name");
    By password = By.id ("edit-pass");
    By submit = By.id ("edit-submit");
    public static final Logger LOG = Logger.getLogger (DrupalLoginPage.class);

    public DrupalLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public DrupalLoginPage() {
    }


    //Set user name in textbox
    public void setUserName(String strUserName) {
        driver.findElement (userName).clear ();
        driver.findElement (userName).sendKeys (strUserName);
    }

    //Set password in password textbox
    public void setPassword(String strPassword) {
        driver.findElement (password).clear ();
        driver.findElement (password).sendKeys (strPassword);
    }

    //Click on submit button
    public void clickSubmit() {
        driver.findElement (submit).click ();
    }


    public void loginDrupalLoginPage(String strUserName, String strPasword) {


        //Fill user name
        this.setUserName (strUserName);
        //Fill password
        this.setPassword (strPasword);
        //Click Login button
        this.clickSubmit ();
    }
}


