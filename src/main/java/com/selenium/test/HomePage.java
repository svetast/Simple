package com.selenium.test;

import org.openqa.selenium.WebDriver;


public class HomePage extends DrupalLoginPage {

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    //Get the Page name from Home Page
    public String getHomePageTitle() {
        return driver.getTitle ();
    }
}
