package com.selenium.test;

import org.openqa.selenium.WebDriver;

public class UnLoggedPage extends DrupalLoginPage {


    public UnLoggedPage(WebDriver driver) {
        this.driver = this.driver;
    }

    //Get the Page name from UnLoggedPage
    public String getUnLoggedPageTitle() {
        return driver.getTitle ();
    }
}


