package com.selenium.test;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.Logs;
import org.testng.annotations.AfterTest;

import static org.junit.Assert.assertEquals;


public class Drupal {

    private static final String URL =
            "https://www.drupal.org/user/login?destination=home";
    private WebDriver driver;

    private void submitForm() {
        WebElement submit = driver.findElement (By.id ("edit-submit"));
        submit.click ();
    }

    private void sendPassword(String password) {
        WebElement passw = driver.findElement (By.id ("edit-pass"));
        passw.clear ();
        passw.sendKeys (password);
    }

    private void login(String key) {
        WebElement login = driver.findElement (By.id ("edit-name"));
        login.clear ();
        login.sendKeys (key);
    }

    public static final Logger LOG = Logger.getLogger (Drupal.class);


    public void checkSuccess() {

        assertEquals ("Drupal - Open Source CMS | Drupal.org", driver.getTitle ());
    }


    public void chekUnSuccess() throws Exception {
        assertEquals ("Error message\n" +
                        "Sorry, unrecognized username or password. Have you forgotten your password?",
                driver.findElement (By.cssSelector ("div.messages.error")).
                        getText ());
    }

    public void checkEmptyLoginPassword() {
        assertEquals ("Username or email field is required.",
                driver.findElement (By.cssSelector ("div.messages.error > ul > li")).
                        getText ());

        assertEquals ("Password field is required.",
                driver.findElement (By.xpath ("//div[@id='content']/div/ul/li[2]")).
                        getText ());

    }

    public void checkEmptyPassword() {
        assertEquals ("Password field is required.", driver.findElement
                (By.cssSelector ("div.messages.error > ul > li")).getText ());
    }

    public void checkEmptyLogin() {
        assertEquals ("Error message\n" +
                        "Username or email field is required.",
                driver.findElement (By.xpath ("//div[@id='content']/div")).getText ());
    }

    @Before
    public void setUp() {
        PropertyConfigurator.configure ("log4j.properties");
        driver = new FirefoxDriver ();
        driver.get (URL);
    }

    @AfterTest
    public void closeBrowser() {
        Logs logs = driver.manage ().logs ();
        LogEntries logEntries = logs.get (LogType.DRIVER);

        for ( LogEntry logEntry : logEntries ) {
            System.out.println (logEntry.getMessage ());
            if (driver != null) {
                driver.close ();
                driver.quit ();
            }
        }
    }

    @Test
    public void inputSuccess() {
        LOG.info ("Begin login inputSuccess");
        login ("oa.org.ua");
        sendPassword ("12345Q");
        submitForm ();
        checkSuccess ();

    }


    @Test
    public void inputEmptyPassword() throws Exception {
        LOG.info ("Begin login inputEmptyPassword");
        login ("oa.org.ua");
        submitForm ();
        checkEmptyPassword ();

    }


    @Test
    public void inputEmptyLogin() throws Exception {
        LOG.info ("Begin login inputEmptyLogin");
        sendPassword ("12345Q");
        submitForm ();
        checkEmptyLogin ();


    }


    @Test
    public void inputEmptyLoginPassword() throws Exception {
        LOG.info ("Begin login inputEmptyLoginPassword");
        submitForm ();
        checkEmptyLoginPassword ();


    }


    @Test
    public void inputUnSuccess() throws Exception {
        LOG.info ("Begin login inputUnSuccess");
        login ("rama");
        sendPassword ("12345");
        submitForm ();
        chekUnSuccess ();


    }

}











