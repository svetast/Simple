package com.selenium.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.selenium.test.Drupal.LOG;

public class TestDrupalLooginPage {


    WebDriver driver;
    DrupalLoginPage objLogin;
    HomePage objHome;
    UnLoggedPage objUnLogged;

    @BeforeTest
    public void setup() {
        driver = new FirefoxDriver ();
        driver.manage ().timeouts ().implicitlyWait (10, TimeUnit.SECONDS);
        driver.get ("https://www.drupal.org/user/login?destination=home");
    }

    @AfterTest
    public void closeBrowser() {


    }


    /**
     * This test case will demonstraid login in https://www.drupal.org/user/login?destination=home
     * Login to application
     * Verify the home page using getTitle
     */
    @Test
    public void inputEmptyLoginPassword() {
        //Create Login Page object
        objLogin = new DrupalLoginPage (driver);
        //login to application
        objLogin.setUserName ("");
        objLogin.setPassword ("");
        objLogin.clickSubmit ();
        // go to the next page (resultPage)
        objUnLogged = new UnLoggedPage (driver);
        //Verify UnLoggedPage when user doesn't login
        Assert.assertTrue (driver.getTitle ().contains ("Log in | Drupal.org"));

        LOG.error ("ERROR");
    }


    @Test
    public void inputEmptyPassword() {
        //Create Login Page object
        objLogin = new DrupalLoginPage (driver);
        //login to application
        objLogin.setUserName ("oa.org.ua");
        objLogin.setPassword ("");
        objLogin.clickSubmit ();
        // go to the next page (resultPage)
        objUnLogged = new UnLoggedPage (driver);
        //Verify UnLoggedPage when user doesn't login
        Assert.assertTrue (driver.getTitle ().contains ("Log in | Drupal.org"));
        LOG.error ("ERROR");

    }

    @Test
    public void inputEmptyLogin() {
        //Create Login Page object
        objLogin = new DrupalLoginPage (driver);
        //login to application
        objLogin.setUserName ("");
        objLogin.setPassword ("12345Q");
        objLogin.clickSubmit ();
        // go to the next page (resultPage)
        objUnLogged = new UnLoggedPage (driver);
        //Verify UnLoggedPage when user doesn't login
        Assert.assertTrue (driver.getTitle ().contains ("Log in | Drupal.org"));
        LOG.error ("ERROR");
    }

    @Test
    public void inputUnSuccess() {
        //Create Login Page object
        objLogin = new DrupalLoginPage (driver);
        //login to application
        objLogin.setUserName ("rama");
        objLogin.setPassword ("123");
        objLogin.clickSubmit ();
        // go to the next page (resultPage)
        objUnLogged = new UnLoggedPage (driver);
        //Verify UnLoggedPage when user doesn't login
        Assert.assertTrue (driver.getTitle ().contains ("Log in | Drupal.org"));
        LOG.error ("ERROR");

    }

    @Test
    public void inputSuccess() {
        //Create Login Page object
        objLogin = new DrupalLoginPage (driver);
        //login to application
        objLogin.setUserName ("oa.org.ua");
        objLogin.setPassword ("12345Q");
        objLogin.clickSubmit ();

        // go to the next page (resultPage)
        objHome = new HomePage (driver);
        //Verify home page
        Assert.assertTrue (objHome.getHomePageTitle ().contains ("Drupal - Open Source CMS | Drupal.org"));

        LOG.info ("Begin login inputSuccess");
    }


}
